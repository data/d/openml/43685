# OpenML dataset: Dogecoin--historical-data-(jan2018---feb2021)

https://www.openml.org/d/43685

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Dogecoin is making news as well as a little profit these days although it may seem like its new in the market but it has been around quite a while now. I have tried to collect historical data of Dogecoin from year jan-2018 to feb-2021
Inspiration
You can try to do time series analysis on this data. Good luck and have fun.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43685) of an [OpenML dataset](https://www.openml.org/d/43685). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43685/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43685/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43685/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

